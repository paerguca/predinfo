package predinfo_bd_mssql

import (
	"bytes"
	"os/exec"
	"strings"
)

type DriverStr struct {
	Driver string
}

func (e *DriverStr) ExtenRequest() []string {
	//cmd := exec.Command("python2.7")
	cmd := exec.Command(e.Driver)
	r := strings.NewReader(`#!/usr/bin/python
import pymssql
import os
import subprocess
import time

conn = pymssql.connect(host='info-db', user='sa', password='Sql2020', database='INFOREGX')
cursor = conn.cursor()

conn.autocommit(True)
cursor.execute('select CODEVENTO, EVENTO, AREA, ANEXO, ACTIVO from TBL_PREDICEVENTO where ACTIVO = 1')

query = cursor.fetchall()
for idx, q in enumerate(query):
  codevento, evento, area, anexo, estado = q
  print (anexo)
`)
	var o bytes.Buffer

	cmd.Stdin = r
	cmd.Stdout = &o

	if err := cmd.Run(); err != nil {
		panic(err)
	}

	anexosSlice := strings.Fields(o.String())

	return anexosSlice
}
