package main

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"strings"
)

func main() {
	var code = `#!/usr/bin/python
import pymssql
import os
import sys
import subprocess
import time

conn = pymssql.connect(host='info-db', user='sa', password='Sql2020', database='INFOREGX')
cursor = conn.cursor()

conn.autocommit(True)
cursor.callproc('usp_s_telefono_x_anexo',(sys.argv[1],))

for row in cursor:
    codficha, codevento, telefono = row
    if telefono:
      print telefono
`
	cmd := exec.Command("python2.7", "-c", code, os.Args[1])
	var o bytes.Buffer
	cmd.Stdout = &o

	if err := cmd.Run(); err != nil {
		panic(err)
	}

	var tel string = o.String()
	tel = strings.Replace(tel, "\n", "", -1)
	fmt.Print(tel)
}
