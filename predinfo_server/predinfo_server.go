package predinfo_server

import (
	"io"
	"log"
	"os"
	"strings"

	"gitlab.com/paerguca/predinfo/predinfopb"
)

type PredinfoServer struct {
	predinfopb.UnimplementedCallFileServiceServer
}

// define CallService
func (*PredinfoServer) Download(req *predinfopb.CallFileRequest, stream predinfopb.CallFileService_DownloadServer) error {

	telnumCall := req.GetCall().GetTelnum()
	extenCall := req.GetCall().GetExten()
	ttrunkCall := req.GetCall().GetTtrunk()
	contextCall := req.GetCall().GetContext()
	clientNameCall := req.GetCall().GetClient()
	fileNameCall := req.GetFileName()

	callUid := strings.Split(fileNameCall, "_")[1] // Usamos el UID enviado por el cliente gRPC en el nombre del archivo de llamada .call

	log.Printf("GRPC SERVER ::: Call function was invoked by %v with %v ID: %s\n", clientNameCall, req, callUid)

	file, err := os.Create(fileNameCall + ".call")
	if err != nil {
		log.Fatalf("GRPC SERVER ::: Error en creación de filecall : %v", err)
	}

	defer file.Close()

	_, err2 := file.WriteString("Channel: " + ttrunkCall + "/" + telnumCall + "\nWaitTime: 100\nCallerID: predi_" + telnumCall + "_" + extenCall + "_" + callUid + " <" + telnumCall + ">\nContext: " + contextCall + "\nExtension: " + extenCall + "\nPriority: 1\n")
	if err2 != nil {
		log.Fatalf("GRPC SERVER ::: Error durante la edición del filecall : %v", err2)
	}

	path := fileNameCall + ".call"
	fileInfo, err := os.Stat(path)
	if err != nil {
		return err
	}
	fileSize := fileInfo.Size()

	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()
	var totalBytesStreamed int64

	//buff := make([]byte, bufferSize)
	for totalBytesStreamed < fileSize {
		filecall := make([]byte, 1024)
		bytesRead, err := f.Read(filecall)
		if err == io.EOF {
			log.Println("GRPC SERVER ::: Download complete")
			break
		}
		if err != nil {
			return err
		}

		if err := stream.Send(&predinfopb.CallFileResponse{
			Filecall: filecall,
		}); err != nil {
			log.Println("GRPC SERVER ::: Error while sending callfile:", err)
			return err
		}
		totalBytesStreamed += int64(bytesRead)
	}
	return nil
}
