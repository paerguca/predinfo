package main

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/paerguca/predinfo/predinfopb"
	"google.golang.org/grpc"
)

func main() {

	if len(os.Args) != 6 {
		fmt.Println("GRPC CLIENT ::: Usage: ", os.Args[0], "PHONE-NUMBER", "EXTENSION", "TRUNK", "CONTEXT", "FILENAME")
		return
	}

	telnum := os.Args[1]
	exten := os.Args[2]
	ttrunk := os.Args[3]
	contxt := os.Args[4]
	filename := os.Args[5]

	cc, err := grpc.Dial("ec2-13-58-210-108.us-east-2.compute.amazonaws.com:50051", grpc.WithInsecure())

	if err != nil {
		log.Fatalf("GRPC CLIENT ::: Could not connect: %v\n", err)
	}

	defer cc.Close()

	c := predinfopb.NewCallFileServiceClient(cc)

	if err := download(telnum, exten, ttrunk, contxt, filename, c); err != nil {
		log.Fatalf("GRPC CLIENT ::: failed to download : %v\n", err)
	}
	fmt.Printf("GRPC CLIENT ::: ==== Proceso gRPC exitoso, originando llamada %s\n", filename)
}

func download(tlf string, ext string, tnk string, ctx string, fln string, c predinfopb.CallFileServiceClient) error {
	//fmt.Println("GRPC CLIENT ::: Enviando parametros al servidor para generacion de .call")

	req := &predinfopb.CallFileRequest{
		Call: &predinfopb.CallFileFormat{
			Telnum:  tlf,
			Exten:   ext,
			Ttrunk:  tnk,
			Context: ctx,
			Client:  "CableClub",
		},
		FileName: fln,
	}

	stream, err := c.Download(context.Background(), req)
	if err != nil {
		log.Fatalf("GRPC CLIENT ::: Error while calling CALL RPC : %v\n", err)
		return err
	}

	var downloaded int64
	var buffer bytes.Buffer

	for {
		res, err := stream.Recv()
		if err == io.EOF {
			if err := ioutil.WriteFile("/var/spool/asterisk/outgoing/"+req.FileName+".call", buffer.Bytes(), 0750); err != nil {
				//if err := ioutil.WriteFile(req.FileName+".call", buffer.Bytes(), 0750); err != nil {
				return err
			}
			break
		}
		if err != nil {
			buffer.Reset()
			return err
		}

		filecall := res.GetFilecall()
		filecallSize := len(filecall)
		downloaded += int64(filecallSize)

		buffer.Write(filecall)
		//fmt.Printf("GRPC CLIENT ::: Got new chunk with data: %s\n", res.Filecall)
	}
	return nil
}
