package main

import (
	"log"
	"net"
	"os"

	"gitlab.com/paerguca/predinfo/predinfo_server"
	"gitlab.com/paerguca/predinfo/predinfopb"
	"google.golang.org/grpc"
)

func main() {

	// Setting LOGS to a custom file
	LOG_FILE := "/home/ec2-user/go/src/gitlab.com/paerguca/predinfo/predinfo-grpc-server.log"
	logFile, err := os.OpenFile(LOG_FILE, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		log.Panic(err)
	}
	defer logFile.Close()

	// Setting log out
	log.SetOutput(logFile)

	// Setting date, filename and line number
	log.SetFlags(log.Lshortfile | log.LstdFlags)

	// Asign port
	Port := os.Getenv("PORT")
	if Port == "" {
		Port = "50051" // default port set to 50051 if PORT is not set in env
	}

	// init listener
	listen, err := net.Listen("tcp", ":"+Port)

	if err != nil {
		log.Fatalf("GRPC SERVER LISTEN ::: Could not listen @ %v :: %v", Port, err)
	}
	log.Println("GRPC SERVER LISTEN ::: Listening @ : " + Port)

	// gRPC server instance
	grpcserver := grpc.NewServer()
	predinfopb.RegisterCallFileServiceServer(grpcserver, &predinfo_server.PredinfoServer{})

	// gRPC listen and serve
	err = grpcserver.Serve(listen)
	if err != nil {
		log.Fatalf("GRPC SERVER LISTEN ::: Error al iniciar servidor gRPC :: %v", err)
	}
}
