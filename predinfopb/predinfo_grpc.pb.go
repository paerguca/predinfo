// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package predinfopb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// CallFileServiceClient is the client API for CallFileService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type CallFileServiceClient interface {
	// Stream Server API
	Download(ctx context.Context, in *CallFileRequest, opts ...grpc.CallOption) (CallFileService_DownloadClient, error)
}

type callFileServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewCallFileServiceClient(cc grpc.ClientConnInterface) CallFileServiceClient {
	return &callFileServiceClient{cc}
}

func (c *callFileServiceClient) Download(ctx context.Context, in *CallFileRequest, opts ...grpc.CallOption) (CallFileService_DownloadClient, error) {
	stream, err := c.cc.NewStream(ctx, &CallFileService_ServiceDesc.Streams[0], "/predinfo.CallFileService/Download", opts...)
	if err != nil {
		return nil, err
	}
	x := &callFileServiceDownloadClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type CallFileService_DownloadClient interface {
	Recv() (*CallFileResponse, error)
	grpc.ClientStream
}

type callFileServiceDownloadClient struct {
	grpc.ClientStream
}

func (x *callFileServiceDownloadClient) Recv() (*CallFileResponse, error) {
	m := new(CallFileResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// CallFileServiceServer is the server API for CallFileService service.
// All implementations must embed UnimplementedCallFileServiceServer
// for forward compatibility
type CallFileServiceServer interface {
	// Stream Server API
	Download(*CallFileRequest, CallFileService_DownloadServer) error
	mustEmbedUnimplementedCallFileServiceServer()
}

// UnimplementedCallFileServiceServer must be embedded to have forward compatible implementations.
type UnimplementedCallFileServiceServer struct {
}

func (UnimplementedCallFileServiceServer) Download(*CallFileRequest, CallFileService_DownloadServer) error {
	return status.Errorf(codes.Unimplemented, "method Download not implemented")
}
func (UnimplementedCallFileServiceServer) mustEmbedUnimplementedCallFileServiceServer() {}

// UnsafeCallFileServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to CallFileServiceServer will
// result in compilation errors.
type UnsafeCallFileServiceServer interface {
	mustEmbedUnimplementedCallFileServiceServer()
}

func RegisterCallFileServiceServer(s grpc.ServiceRegistrar, srv CallFileServiceServer) {
	s.RegisterService(&CallFileService_ServiceDesc, srv)
}

func _CallFileService_Download_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(CallFileRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(CallFileServiceServer).Download(m, &callFileServiceDownloadServer{stream})
}

type CallFileService_DownloadServer interface {
	Send(*CallFileResponse) error
	grpc.ServerStream
}

type callFileServiceDownloadServer struct {
	grpc.ServerStream
}

func (x *callFileServiceDownloadServer) Send(m *CallFileResponse) error {
	return x.ServerStream.SendMsg(m)
}

// CallFileService_ServiceDesc is the grpc.ServiceDesc for CallFileService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var CallFileService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "predinfo.CallFileService",
	HandlerType: (*CallFileServiceServer)(nil),
	Methods:     []grpc.MethodDesc{},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "Download",
			Handler:       _CallFileService_Download_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "predinfopb/predinfo.proto",
}
