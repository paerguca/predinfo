# Install Go v1.16.3
```
pwd
wget https://golang.org/dl/go1.16.3.linux-amd64.tar.gz
tar -C /usr/local/ -zxvf go1.16.3.linux-amd64.tar.gz
rm -rf go1.16.3.linux-amd64.tar.gz
vim ~/.bashrc
```
### Bashrc
```
# Golang
export PATH=$PATH:/usr/local/go/bin
```
source .bashrc

# Install Protoc 3.15.5 (for gRPC API)
```
PB_REL="https://github.com/protocolbuffers/protobuf/releases"
curl -LO $PB_REL/download/v3.15.5/protoc-3.15.5-linux-x86_64.zip
unzip protoc-3.15.5-linux-x86_64.zip -d $HOME/.local
rm -rf protoc-3.15.5-linux-x86_64.zip
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
vim ~/.bashrc
```
### Bashrc
```
# Protoc
export PATH="$PATH:$HOME/.local/bin"
```

# Testing Go and Protoc
```
go version
protoc --version
```
# Prerequisites
Python 2.7.5
module pymssql

## Client Installation
### Asterisk
1. Crear usuario AMI : # vim /etc/asterisk/manager.conf
```
[user_asterisk_manager]
secret = password_asterisk_manager
permit=127.0.0.1/255.255.255.0
read = system,call,log,verbose,agent,user,config,dtmf,reporting,cdr,dialplan,command,all
write = system,call,log,verbose,command,agent,user,all
```
2. Descargar repositorio del proyecto desde GITLAB y generar binarios para ambiente de produccion
```
# cd go/src/gitlab.com/paerguca
# git clone https://gitlab.com/paerguca/predinfo.git
# cd predinfo
# mkdir go-bin
# cd predinfo_bd_mssql/predice_telefono
# go build predinfo_bd_mssql_tlf.go
# mv predinfo_bd_mssql_tlf ../../go-bin
# cd ..
# cd ..
# cd predinfo_client
# go build predinfo_client.go
# mv predinfo_client ../go-bin
# cd ..
# cd predinfo_events/
# go build predinfo_events.go 
```
Finalmente borrar todos las carpetas y archivos menos los go.mod, go.sum, la carpeta sh-bin y predinfo_events
3. Copiar los binarios al servidor asterisk (equipo dev)
```
$ scp -P 14586 apps-go/gitlab.com/paerguca/predinfo/go-bin/predinfo_client root@2.tcp.ngrok.io:/root/go/src/gitlab.com/paerguca/predinfo/go-bin/
$ scp -P 14586 apps-go/gitlab.com/paerguca/predinfo/go-bin/predinfo_bd_mssql_tlf root@2.tcp.ngrok.io:/root/go/src/gitlab.com/paerguca/predinfo/go-bin/
$ scp -P 14586 apps-go/gitlab.com/paerguca/predinfo/go-bin/predinfo_events root@2.tcp.ngrok.io:/root/go/src/gitlab.com/paerguca/predinfo/
```
4. Generar Systemd: Crear archivo y editar: # vim sudo /etc/systemd/system/predinfo.service
```
[Unit]
Description=Go gRPC Client - Predinfo
After=multi-user.target

[Service]
User=root
Group=root
ExecStart=/root/go/src/gitlab.com/paerguca/predinfo/predinfo_events/predinfo_events

[Install]
WantedBy=multi-user.target
```
5. Cargar las variables de entorno cuando systemd ejecute el binario: # vim /etc/systemd/system/predinfo.service.d/envAMI.conf
```
[Service]
Environment="USER_AMI=user_asterisk_manager"
Environment="PASS_AMI=password_asterisk_manager"
```
6. Iniciar servicio y activar el autoiniciado en caso haya un reboot del servidor.
```
# sudo systemctl start predinfo.service
# sudo systemctl enable predinfo.service
```
7. Log rotate
$ sudo vim /etc/logrotate.d/predinfo.conf
```
/root/go/src/gitlab.com/paerguca/predinfo/predinfo.log {
    su root root
    weekly
    rotate 3
    size 5M
    dateext
    dateformat -%d%m%Y
    notifempty
    mail paerguca.brain@gmail.com
}
```
8. Alias: # vim .bashrc
```
# Custom alias
alias predinfo="tail -f /root/go/src/gitlab.com/paerguca/predinfo/predinfo.log"

# source .bashrc
```
9. Crontab (limpiar registros en DBAst)
$ sudo crontab -e
```
0 2 * * * asterisk -rx "database deltree PRED"
```

## Server Installation (AWS EC2)

1. Descargar repositorio del proyecto desde GITLAB y generar binarios para ambiente de produccion
```
$ cd go/src/gitlab.com/paerguca
$ git clone https://gitlab.com/paerguca/predinfo.git 
$ cd predinfo/
$ go build server.go 
```
Finalmente borrar todos las carpetas y archivos menos los go.mod, go.sum
3. Crear archivo y editar: $ vim sudo /etc/systemd/system/predinfo_server.service
```
[Unit]
Description=Go gRPC Server
After=multi-user.target

[Service]
User=root
Group=root
ExecStart=/home/ec2-user/go/src/gitlab.com/paerguca/predinfo/server

[Install]
WantedBy=multi-user.target
```
4. Iniciar servicio y activar el autoiniciado en caso haya un reboot del servidor.
```
$ sudo systemctl start predinfo_server.service
$ sudo systemctl enable predinfo_server.service
```
5. Log rotate
$ sudo vim /etc/logrotate.d/predinfo_server.conf
```
/home/ec2-user/go/src/gitlab.com/paerguca/predinfo/predinfo-grpc-server.log {
    su ec2-user ec2-user
    weekly
    rotate 3
    size 5M
    dateext
    dateformat -%d%m%Y
    notifempty
    mail paerguca.brain@gmail.com
}
```
6. Alias: 
```
$ sudo vim .bashrc
# Custom alias
alias predinfo-server="tail -f /home/ec2-user/go/src/gitlab.com/paerguca/predinfo/predinfo-grpc-server.log"

$ source .bashrc
```
6. Crontab (limpiar archivos predicall_*)
$ sudo crontab -e
```
0 2 * * * rm -rf /predicall_*
```

# Log
### Server
```
$ predinfo-server
```

### Client
```
# predinfo
```
 

