package main

import (
	"bytes"
	"fmt"
	"log"
	"math/rand"
	"os"
	"os/exec"
	"strings"

	"github.com/ivahaev/amigo"
	"gitlab.com/paerguca/predinfo/predinfo_bd_mssql"
)

func FindAxo(sliceAxos []string, axo string) (int, bool) {
	for i, item := range sliceAxos {
		if item == axo {
			return i, true
		}
	}
	return -1, false
}

func DiffAxos(slice1 []string, slice2 []string) []string {
	var diff []string

	// Loop two times, first to find slice1 strings not in slice2,
	// second loop to find slice2 strings not in slice1
	//for i := 0; i < 2; i++ {
	for _, s1 := range slice1 {
		found := false
		for _, s2 := range slice2 {
			if s1 == s2 {
				found = true
				break
			}
		}
		// String not found. We add it to return slice
		if !found {
			diff = append(diff, s1)
		}
	}
	// Swap the slices, only if it was the first loop
	//if i == 0 {
	//slice1, slice2 = slice2, slice1
	//}
	//}

	return diff
}

// Creating hanlder functions
func UbicaPredictiva(m map[string]string) {
	callIDName := m["CallerIDName"]
	prediCall := strings.Split(callIDName, "_") // Slice de strings donde: [0]:"predi", [1]:"telefono", [2]: "anexo"

	if prediCall[0] != "predi" {
		//Para ubicar llamadas de predictiva solo en aquellos anexos que estan en llamada no generada por la predicitiva.
		cmdGetAxos := exec.Command("/bin/sh", "/root/go/src/gitlab.com/paerguca/predinfo/sh-bin/dbAstAxosPred.sh", "get") // recuperando string de anexos asigandos a la predictiva [anexo1_anexo2_anexo3_anexo4...]
		var oInv bytes.Buffer
		cmdGetAxos.Stdout = &oInv
		if err := cmdGetAxos.Run(); err != nil {
			panic(err)
		}

		var strAxosPred string = oInv.String()
		strAxosPred = strings.Replace(strAxosPred, "\n", "", -1)

		slcAxosPred := strings.Split(strAxosPred, "_")

		anexo := m["CallerIDNum"]

		_, axoFound := FindAxo(slcAxosPred, anexo)

		if axoFound {

			cmd := exec.Command("/bin/sh", "/root/go/src/gitlab.com/paerguca/predinfo/sh-bin/extenAvail.sh", anexo) // numero de lineas tomadas por el anexo: 0: disponible, 1: 1 linea tomada por el anexo, 2: 2 lineas tomadas, etc...

			var o bytes.Buffer
			cmd.Stdout = &o
			if err := cmd.Run(); err != nil {
				panic(err)
			}

			var lines string = o.String()
			lines = strings.Replace(lines, "\n", "", -1)

			if lines != "0" {
				log.Printf("Anexo %v ocupado, lineas tomadas por el anexo: %v\n", anexo, lines)
			} else {
				//fmt.Println("Pide telefono en UBICA desde llamada NO PREDI")
				cmdTlf := exec.Command("/root/go/src/gitlab.com/paerguca/predinfo/go-bin/predinfo_bd_mssql_tlf", anexo)
				outTlf, err := cmdTlf.CombinedOutput()
				if err != nil {
					log.Fatal(err)
				}

				if string(outTlf) != "" {
					filename := fmt.Sprintf("predicall_%d", rand.Intn(1e6))

					cmd := exec.Command("/root/go/src/gitlab.com/paerguca/predinfo/go-bin/predinfo_client", string(outTlf), anexo, "SIP/trunk-cvg", "anexos", filename) // Ejecuta el client gRPC cuando se detecte un evento definido.
					var out bytes.Buffer
					var stderr bytes.Buffer
					cmd.Stdout = &out
					cmd.Stderr = &stderr
					er := cmd.Run()
					if er != nil {
						log.Println(fmt.Sprint(er) + ": " + stderr.String())
						return
					}
					log.Printf("LLAMADA ::: %s <---> %v\n", outTlf, anexo)
					log.Println("====  ", out.String())
				}
			}
		}
	} else {
		// Ubica llamadas a anexos originados por predictiva.
		//fmt.Printf("Hangup event received: %v\n", m)
		telefono, axo := prediCall[1], prediCall[2]

		if telefono == m["CallerIDNum"] { //Se detectaron 2 hangup, uno del anexo y otro del telefono destino. Se define el hangup del telefono como el Handler.
			cmdPred := exec.Command("/bin/sh", "/root/go/src/gitlab.com/paerguca/predinfo/sh-bin/extenAvail.sh", axo) // numero de lineas tomadas por el anexo: 0: disponible, 1: 1 linea tomada por el anexo, 2: 2 lineas tomadas, etc...

			var o bytes.Buffer
			cmdPred.Stdout = &o
			if err := cmdPred.Run(); err != nil {
				panic(err)
			}

			var linesPred string = o.String()
			linesPred = strings.Replace(linesPred, "\n", "", -1)

			if linesPred != "0" {
				log.Printf("Anexo %v ocupado, lineas tomadas por el anexo: %v\n", axo, linesPred)
			} else {
				//fmt.Println("Pide telefono en UBICA desde llamada PREDI")
				cmdTlf := exec.Command("/root/go/src/gitlab.com/paerguca/predinfo/go-bin/predinfo_bd_mssql_tlf", axo)
				outTlf, err := cmdTlf.CombinedOutput()
				if err != nil {
					log.Fatal(err)
				}

				if string(outTlf) != "" {
					filename := fmt.Sprintf("predicall_%d", rand.Intn(1e6))

					cmd := exec.Command("/root/go/src/gitlab.com/paerguca/predinfo/go-bin/predinfo_client", string(outTlf), axo, "SIP/trunk-cvg", "anexos", filename) // Ejecuta el client gRPC cuando se detecte un evento definido.
					var out bytes.Buffer
					var stderr bytes.Buffer
					cmd.Stdout = &out
					cmd.Stderr = &stderr
					er := cmd.Run()
					if er != nil {
						log.Println(fmt.Sprint(er) + ": " + stderr.String())
						return
					}
					log.Printf("LLAMADA ::: %s <---> %v\n", outTlf, axo)
					log.Println(out.String())
				}
			}

			// Codigo para guardar en MSSQL llamadas no respondidas por el cliente (solo originadas por la predictiva)
			if m["Cause"] != "16" { // Cause: 16 indica que llamada fue respondida, esta logica es resuelta en SavingCallAnsweredPred
				if m["Cause"] != "17" {

					log.Printf("LLAMADA ::: %v <---> %v [TERMINADA/NO RESPONDIDA] - %v\n", prediCall[1], prediCall[2], prediCall[1])
					var code = `#!/usr/bin/python
import pymssql
import os
import sys
import re
import time
import subprocess

#unq_string = sys.argv[1] # trae  disp_dest[0], uniqueid_dest[1], disp_agn[2], uniqueid_agn[3]
#arr = unq_string.split(",")
lst = []
#print 'arreglo traido de eventos de llamada no contestada, abandonada: '+unq_string
conn = pymssql.connect(host='info-db', user='sa', password='Sql2020', database='INFOREGX')
cur = conn.cursor()
data = [
		(sys.argv[2],sys.argv[1]),
] # telefono, uniqueid
stmt = "INSERT INTO TRAZA_PREDICTIVA VALUES (%s, %s)"

cur.executemany(stmt, data)
conn.commit()
conn.close()
print "saved"`
					cmdSave := exec.Command("python2.7", "-c", code, prediCall[1], m["UniqueID"])
					var o bytes.Buffer
					cmdSave.Stdout = &o

					if err := cmdSave.Run(); err != nil {
						panic(err)
					}

					var statusSave string = o.String()
					statusSave = strings.Replace(statusSave, "\n", "", -1)
					log.Printf("Guardado en BD MSSQL: %s\n", statusSave)
				}
			}
		}
	}
}

func NewCallerid(m map[string]string) {
	log.Println("event NewCallerid received: ", m)

}

func TraceUniqueID(m map[string]string) {
	if strings.Split(m["CallerIDName"], "_")[0] == "predi" {
		//fmt.Printf("event received: %v\n", m)
		cmdPut := exec.Command("/bin/sh", "/root/go/src/gitlab.com/paerguca/predinfo/sh-bin/dbAstTraceUid.sh", "put", m["UniqueID"], m["UniqueID"]) // capturando temporalmente en base al uniqueid origen(src) los valores de telefono, anexo, idcall y uniqueid destino(dst)

		var o bytes.Buffer
		cmdPut.Stdout = &o
		if err := cmdPut.Run(); err != nil {
			panic(err)
		}

		var ok string = o.String()
		ok = strings.Replace(ok, "\n", "", -1)

		log.Println(ok)
		if ok != "successfully" {
			log.Println("Error al ejecutar script dbAstTraceUid.sh: ", ok)
		}
	}
}

func DefHandler(m map[string]string) {

	cmdGet := exec.Command("/bin/sh", "/root/go/src/gitlab.com/paerguca/predinfo/sh-bin/dbAstTraceUid.sh", "get", m["UniqueID"]) // recuperando string de calleridname [predi_telefono_anexo_idcall_uniqueidDest].
	var oInv bytes.Buffer
	cmdGet.Stdout = &oInv
	if err := cmdGet.Run(); err != nil {
		panic(err)
	}

	var UidTrace string = oInv.String()
	UidTrace = strings.Replace(UidTrace, "\n", "", -1)

	if UidTrace != "entry" {
		if m["UniqueID"] == UidTrace {
			log.Printf("event TRACE received: %v\n", m)
		}
	}

}

// Guardar las llamadas respondidas por el cliente que pueden ser atendidas o no atendidas (solo originadas por la predicitva)
func SavingCallAnsweredPred(m map[string]string) {

	idPredCall := strings.Split(m["CallerIDName"], "_") // [0]: "predi", [1]: "telefono", [2]: "anexo", [3]: "idcall"

	if idPredCall[0] == "predi" {
		switch m["SubEvent"] {
		case "Begin":
			strCall := fmt.Sprintf("%v_%v", m["CallerIDName"], m["DestUniqueID"])

			cmdPut := exec.Command("/bin/sh", "/root/go/src/gitlab.com/paerguca/predinfo/sh-bin/dbAstzz.sh", "put", m["UniqueID"], strCall) // capturando temporalmente en base al uniqueid origen(src) los valores de telefono, anexo, idcall y uniqueid destino(dst)

			var o bytes.Buffer
			cmdPut.Stdout = &o
			if err := cmdPut.Run(); err != nil {
				panic(err)
			}

			var ok string = o.String()
			ok = strings.Replace(ok, "\n", "", -1)

			if ok != "successfully" {
				log.Println("Error al ejecutar script dbAst.sh: ", ok)
			}

		default:
			log.Printf("Unknown Dial SubEvent: %s\n", m["SubEvent"])
		}
	}

	if m["SubEvent"] == "End" { // El evento End no tiene campo CallerIDName
		cmdInv := exec.Command("/bin/sh", "/root/go/src/gitlab.com/paerguca/predinfo/sh-bin/dbAstzz.sh", "get", m["UniqueID"]) // recuperando string de calleridname [predi_telefono_anexo_idcall_uniqueidDest].
		var oInv bytes.Buffer
		cmdInv.Stdout = &oInv
		if err := cmdInv.Run(); err != nil {
			panic(err)
		}

		var strCallIdName string = oInv.String()
		strCallIdName = strings.Replace(strCallIdName, "\n", "", -1)

		if strCallIdName != "entry" {

			idCall := strings.Split(strCallIdName, "_")

			switch m["DialStatus"] {
			case "CANCEL":
				// Ingresar codigo para grabar llamada en MSSQL - abadonada por cliente
				var code = `#!/usr/bin/python
import pymssql
import os
import sys
import re
import time
import subprocess

#unq_string = sys.argv[1] # trae  disp_dest[0], uniqueid_dest[1], disp_agn[2], uniqueid_agn[3]
#arr = unq_string.split(",")
lst = []
#print 'arreglo traido de eventos de llamada no contestada, abandonada: '+unq_string
conn = pymssql.connect(host='info-db', user='sa', password='Sql2020', database='INFOREGX')
cur = conn.cursor()
data = [
		(sys.argv[2],sys.argv[1]),
] # telefono, uniqueid
stmt = "INSERT INTO TRAZA_PREDICTIVA VALUES (%s, %s)"

cur.executemany(stmt, data)
conn.commit()
conn.close()
print "SAVED"`
				cmdSave := exec.Command("python2.7", "-c", code, idCall[1], m["UniqueID"])
				var o bytes.Buffer
				cmdSave.Stdout = &o

				if err := cmdSave.Run(); err != nil {
					panic(err)
				}

				var statusSave string = o.String()
				statusSave = strings.Replace(statusSave, "\n", "", -1)
				log.Printf("HANG UP ::: %v <---> %v [RESPONDIDA] - [NO ATENDIDA] - %v\n", idCall[1], idCall[2], idCall[1])
				log.Printf("SAVING :::: Guardado en TRAZA BD MSSQL: %s!\n", statusSave)

			case "BUSY":
				// Ingresar codigo para grabar llamada en MSSQL - abadonada por anexo
				var code = `#!/usr/bin/python
import pymssql
import os
import sys
import re
import time
import subprocess

#unq_string = sys.argv[1] # trae  disp_dest[0], uniqueid_dest[1], disp_agn[2], uniqueid_agn[3]
#arr = unq_string.split(",")
lst = []
#print 'arreglo traido de eventos de llamada no contestada, abandonada: '+unq_string
conn = pymssql.connect(host='info-db', user='sa', password='Sql2020', database='INFOREGX')
cur = conn.cursor()
data = [
		(sys.argv[2],sys.argv[1]),
] # telefono, uniqueid
stmt = "INSERT INTO TRAZA_PREDICTIVA VALUES (%s, %s)"

cur.executemany(stmt, data)
conn.commit()
conn.close()
print "SAVED"`
				cmdSave := exec.Command("python2.7", "-c", code, idCall[1], m["UniqueID"])
				var o bytes.Buffer
				cmdSave.Stdout = &o

				if err := cmdSave.Run(); err != nil {
					panic(err)
				}

				var statusSave string = o.String()
				statusSave = strings.Replace(statusSave, "\n", "", -1)
				log.Printf("HANG UP ::: %v <---> %v [RESPONDIDA] - [NO ATENDIDA] - %v\n", idCall[1], idCall[2], idCall[2])
				log.Printf("SAVING ::: Guardado en TRAZA BD MSSQL: %s!\n", statusSave)

			case "ANSWER":
				// Ingresar codigo para grabar llamada en MSSQL - exitosa
				var code = `#!/usr/bin/python
import pymssql
import os
import sys
import re
import time
import subprocess

#unq_string = sys.argv[1] # trae  disp_dest[0], uniqueid_dest[1], disp_agn[2], uniqueid_agn[3]
#arr = unq_string.split(",")
lst = []
#print "arreglo traido de eventos: "+unq_string
conn = pymssql.connect(host='info-db', user='sa', password='Sql2020', database='INFOREGX')
cur = conn.cursor()
data = [
  (sys.argv[2],sys.argv[1]),
  (sys.argv[4],sys.argv[3]),
]
stmt = "INSERT INTO TRAZA_PREDICTIVA VALUES (%s, %s)"

cur.executemany(stmt, data)
conn.commit()
conn.close()
print "SAVED"

			`
				cmdSave := exec.Command("python2.7", "-c", code, idCall[1], m["UniqueID"], idCall[2], idCall[4])
				var o bytes.Buffer
				cmdSave.Stdout = &o

				if err := cmdSave.Run(); err != nil {
					panic(err)
				}

				var statusSave string = o.String()
				statusSave = strings.Replace(statusSave, "\n", "", -1)
				log.Printf("HANG UP ::: %v <---> %v [RESPONDIDA] - [ATENDIDA]\n", idCall[1], idCall[2])
				log.Printf("SAVING ::: Guardado en BD MSSQL: %s!\n", statusSave)

			default:
				// Ingresar codigo para grabar llamada en MSSQL - abadonada por otro motivo
				var code = `#!/usr/bin/python
import pymssql
import os
import sys
import re
import time
import subprocess

#unq_string = sys.argv[1] # trae  disp_dest[0], uniqueid_dest[1], disp_agn[2], uniqueid_agn[3]
#arr = unq_string.split(",")
lst = []
#print 'arreglo traido de eventos de llamada no contestada, abandonada: '+unq_string
conn = pymssql.connect(host='info-db', user='sa', password='Sql2020', database='INFOREGX')
cur = conn.cursor()
data = [
		(sys.argv[2],sys.argv[1]),
] # telefono, uniqueid
stmt = "INSERT INTO TRAZA_PREDICTIVA VALUES (%s, %s)"

cur.executemany(stmt, data)
conn.commit()
conn.close()
print "SAVED"`
				cmdSave := exec.Command("python2.7", "-c", code, idCall[1], m["UniqueID"])
				var o bytes.Buffer
				cmdSave.Stdout = &o

				if err := cmdSave.Run(); err != nil {
					panic(err)
				}

				var statusSave string = o.String()
				statusSave = strings.Replace(statusSave, "\n", "", -1)
				log.Printf("HANG UP ::: %v <---> %v [RESPONDIDA] - [NO ATENDIDA] - desconocido: %v\n", idCall[1], idCall[2], m["DialStatus"])
				log.Printf("SAVING ::: Guardado en BD MSSQL: %s!\n", statusSave)
			}

		}

	}

	//fmt.Printf("dialEnd event received: %v\n", m)
}

func IniciaPredictiva(m map[string]string) {
	if m["Value"] == "EvtStart" {

		e := predinfo_bd_mssql.DriverStr{
			"python2.7",
		}

		anexos := e.ExtenRequest()
		log.Printf("INICIO DE PREDICITVA ::: Lista de anexos asignados %v\n", anexos)

		if len(anexos) != 0 {

			strAxosIni := strings.Join(anexos, "_")

			cmdPut := exec.Command("/bin/sh", "/root/go/src/gitlab.com/paerguca/predinfo/sh-bin/dbAstAxosPred.sh", "put", strAxosIni) //Guardando temporalmente anexos iniciales asignados a la predictiva
			var o bytes.Buffer
			cmdPut.Stdout = &o
			if err := cmdPut.Run(); err != nil {
				panic(err)
			}

			var ok string = o.String()
			ok = strings.Replace(ok, "\n", "", -1)

			if ok != "successfully" {
				log.Println("Error al ejecutar script dbAstAxosPred.sh: ", ok)
			}

			for _, val := range anexos {
				cmd := exec.Command("/bin/sh", "/root/go/src/gitlab.com/paerguca/predinfo/sh-bin/extenAvail.sh", val) // numero de lineas tomadas por el anexo: 0: disponible, 1: 1 linea tomada por el anexo, 2: 2 lineas tomadas, etc...

				var o bytes.Buffer
				cmd.Stdout = &o
				if err := cmd.Run(); err != nil {
					panic(err)
				}

				var lines string = o.String()
				lines = strings.Replace(lines, "\n", "", -1)

				if lines != "0" {
					log.Printf("Anexo %v ocupado, lineas tomadas por el anexo: %v\n", val, lines)
				} else {
					//fmt.Println("Pide telefono en INICIA")
					cmdTlf := exec.Command("/root/go/src/gitlab.com/paerguca/predinfo/go-bin/predinfo_bd_mssql_tlf", val)
					outTlf, err := cmdTlf.CombinedOutput()
					if err != nil {
						log.Fatal(err)
					}
					log.Printf("LLAMADA ::: %s <---> %v\n", outTlf, val)

					filename := fmt.Sprintf("predicall_%d", rand.Intn(1e6))

					cmd := exec.Command("/root/go/src/gitlab.com/paerguca/predinfo/go-bin/predinfo_client", string(outTlf), val, "SIP/trunk-cvg", "anexos", filename) // Ejecuta el client gRPC cuando se detecte un evento definido.
					var out bytes.Buffer
					var stderr bytes.Buffer
					cmd.Stdout = &out
					cmd.Stderr = &stderr
					er := cmd.Run()
					if er != nil {
						log.Println(fmt.Sprint(er) + ": " + stderr.String())
						return
					}
					log.Println(out.String())
				}

			}
		} else {
			log.Println("INICIO DE PREDICITVA ::: No existen anexos asignados")
		}
	}

	// Cambio en anexos de la predictiva
	if m["Value"] == "EvtChange" {

		e := predinfo_bd_mssql.DriverStr{
			"python2.7",
		}
		slcAxosNuevo := e.ExtenRequest() // Nuevo slice de anexos
		log.Printf("CAMBIO EN PREDICITVA ::: Nueva lista de anexos %v\n", slcAxosNuevo)

		if len(slcAxosNuevo) != 0 {

			strAxosNuevo := strings.Join(slcAxosNuevo, "_")

			cmdPutChg := exec.Command("/bin/sh", "/root/go/src/gitlab.com/paerguca/predinfo/sh-bin/dbAstAxosPred.sh", "put", strAxosNuevo) //Guardando temporalmente nuevos anexos asignados a la predictiva como string unidos por "_"
			var och bytes.Buffer
			cmdPutChg.Stdout = &och
			if err := cmdPutChg.Run(); err != nil {
				panic(err)
			}

			var okCh string = och.String()
			okCh = strings.Replace(okCh, "\n", "", -1)

			if okCh != "successfully" {
				log.Println("Error al ejecutar script dbAstAxosPred.sh: ", okCh)
			}

			cmdGetAxos := exec.Command("/bin/sh", "/root/go/src/gitlab.com/paerguca/predinfo/sh-bin/dbAstAxosPred.sh", "get") // recuperando string de anexos asigandos a la predictiva [anexo1_anexo2_anexo3_anexo4...].
			var oInv bytes.Buffer
			cmdGetAxos.Stdout = &oInv
			if err := cmdGetAxos.Run(); err != nil {
				panic(err)
			}

			var strAxosPred string = oInv.String()
			strAxosPred = strings.Replace(strAxosPred, "\n", "", -1)

			slcAxos := strings.Split(strAxosPred, "_") // Slice de anexos iniciales

			slcAxosDif := DiffAxos(slcAxosNuevo, slcAxos) // Slice anexos nuevos agregados a la predictiva

			if len(slcAxosDif) != 0 {
				//fmt.Printf("CAMBIO: Nueva lista de anexos para atencion: %v", slcAxosNuevo)

				for _, valChg := range slcAxosDif {

					cmdTlfChg := exec.Command("/root/go/src/gitlab.com/paerguca/predinfo/go-bin/predinfo_bd_mssql_tlf", valChg)
					outTlfChg, err := cmdTlfChg.CombinedOutput()
					if err != nil {
						log.Fatal(err)
					}
					log.Printf("LLAMADA ::: %s <---> %v\n", outTlfChg, valChg)

					filenameChg := fmt.Sprintf("predicall_%d", rand.Intn(1e6))

					cmdChg := exec.Command("/bin/sh", "/root/go/src/gitlab.com/paerguca/predinfo/sh-bin/extenAvail.sh", valChg) // numero de lineas tomadas por el anexo: 0: disponible, 1: 1 linea tomada por el anexo, 2: 2 lineas tomadas, etc...

					var oChg bytes.Buffer
					cmdChg.Stdout = &oChg
					if err := cmdChg.Run(); err != nil {
						panic(err)
					}

					var linesChg string = oChg.String()
					linesChg = strings.Replace(linesChg, "\n", "", -1)

					if linesChg != "0" {
						log.Printf("Anexo %v ocupado, lineas tomadas por el anexo: %v\n", valChg, linesChg)
					} else {

						cmdCh := exec.Command("/root/go/src/gitlab.com/paerguca/predinfo/go-bin/predinfo_client", string(outTlfChg), valChg, "SIP/trunk-cvg", "anexos", filenameChg) // Ejecuta el client gRPC cuando se detecte un evento definido.
						var out bytes.Buffer
						var stderr bytes.Buffer
						cmdCh.Stdout = &out
						cmdCh.Stderr = &stderr
						er := cmdCh.Run()
						if er != nil {
							log.Println(fmt.Sprint(er) + ": " + stderr.String())
							return
						}
						log.Println("====  ", out.String())
					}

				}
			} else {
				log.Printf("CAMBIO EN PREDICITVA ::: Misma lista de anexos %v\n", slcAxosNuevo)
			}
		} else {
			log.Println("CAMBIO EN PREDICITVA ::: No se detectan anexos asignados")
		}
	}

}

func main() {

	// Setting LOGS to a custom file
	LOG_FILE := "/root/go/src/gitlab.com/paerguca/predinfo/predinfo.log"
	logFile, err := os.OpenFile(LOG_FILE, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		log.Panic(err)
	}
	defer logFile.Close()

	// Setting log out
	log.SetOutput(logFile)

	// Setting date, filename and line number
	log.SetFlags(log.Lshortfile | log.LstdFlags)

	// Asignar valores de conexión al AMI de Asterisk en las siguientes variables de entorno (USER_AMI, PASS_AMI, HOST_AMI, PORT_AMI)
	// Asign UserAmi
	userAmi := os.Getenv("USER_AMI")

	// Asign PassAmi
	passAmi := os.Getenv("PASS_AMI")

	// Asign HostAmi
	hostAmi := os.Getenv("HOST_AMI")
	// Asign PortAmi
	//portAmi := os.Getenv("PORT_AMI")

	settings := &amigo.Settings{Username: userAmi, Password: passAmi, Host: hostAmi}
	//settings := &amigo.Settings{Username: UserAmi, Password: PassAmi, Host: HostAmi, Port: portAmi}
	a := amigo.New(settings)

	a.Connect()

	// Listen for connection events
	a.On("connect", func(message string) {
		log.Println("Connected", message)
	})
	a.On("error", func(message string) {
		log.Println("Connection error:", message)
	})

	// Registering handler function for event "Hangup"
	a.RegisterHandler("VarSet", IniciaPredictiva)
	a.RegisterHandler("Hangup", UbicaPredictiva)
	a.RegisterHandler("Dial", SavingCallAnsweredPred)

	// Activar estos 2 para Seguimiento de eventos predictiva por UniqueID -> No activar EN REVISION, se utilizo para validar las llamadas que no son respondidas (cliente deja timbrando su telefono) hasta que se cumple el tiempo de 30 segundos segun archivo .call## estas llamadas no son guardadas en BD MSSQL
	//a.RegisterHandler("NewCallerid", NewCallerid)
	//a.RegisterDefaultHandler(DefHandler)

	// Registering default handler function for all events.
	//a.RegisterDefaultHandler(IniciaPredictiva)
	//a.RegisterDefaultHandler(SavingCall)

	// Optionally create channel to receiving all events
	// and set created channel to receive all events
	//c := make(chan map[string]string, 100)
	//a.SetEventChannel(c)

	// Check if connected with Asterisk, will send Action "QueueSummary"
	if a.Connected() {
		result, err := a.Action(map[string]string{"Action": "QueueSummary", "ActionID": "Init"})
		// If not error, processing result. Response on Action will follow in defined events.
		// You need to catch them in event channel, DefaultHandler or specified HandlerFunction
		log.Println(result, err)
	}

	ch := make(chan bool)
	<-ch
}
