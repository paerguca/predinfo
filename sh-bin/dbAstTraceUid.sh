#!/bin/bash

value1=$1 # get o put
value2=$2 # uniqueid origen

commandput="database put PREDTRACE ${value2} ${value2}"
commandget="database get PREDTRACE ${value2}"

if [[ ${value1} = "get" ]]
then
        asterisk -rx "${commandget}" | cut -d " " -f 2
elif [[ ${value1} = "put" ]]
then
        asterisk -rx "${commandput}" | cut -d " " -f 3
fi