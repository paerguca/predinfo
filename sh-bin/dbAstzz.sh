#!/bin/bash

value1=$1 # get o put
value2=$2 # uniqueid origen
value3=$3 # string calleridname [predi_telefono_anexo_idcall]

commandput="database put PRED ${value2} ${value3}"
commandget="database get PRED ${value2}"

if [[ ${value1} = "get" ]]
then
        asterisk -rx "${commandget}" | cut -d " " -f 2
elif [[ ${value1} = "put" ]]
then
        asterisk -rx "${commandput}" | cut -d " " -f 3
fi