package main

import (
	"bytes"
	"fmt"
	"os/exec"
	"strings"

	"gitlab.com/paerguca/predinfo/predinfo_bd_mssql"
)

func main() {

	e := predinfo_bd_mssql.DriverStr{
		"python2.7",
	}

	anexos := e.ExtenRequest()

	for i, val := range anexos {

		cmd := exec.Command("/bin/sh", "./sh-bin/extenAvail.sh", val) // numero de lineas tomadas por el anexo: 0: disponible, 1: 1 linea tomada por el anexo, 2: 2 lineas tomadas, etc...

		var o bytes.Buffer
		cmd.Stdout = &o

		if err := cmd.Run(); err != nil {
			panic(err)
		}

		var lines string = o.String()
		lines = strings.Replace(lines, "\n", "", -1)

		if lines != "0" {
			fmt.Printf("Anexo %v ocupado, lineas tomadas por el anexo: %v\n", val, lines)
		} else {
			fmt.Printf("Realizando llamada %v\n", i+1)
		}

	}

}
