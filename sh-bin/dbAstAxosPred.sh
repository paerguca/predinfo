#!/bin/bash

value1=$1 # get o put
value2=$2 # string de anexos unidos por "_"
#value3=$3 # string calleridname [predi_telefono_anexo_idcall]

commandput="database put PRED ANEXOS ${value2}"
commandget="database get PRED ANEXOS"

if [[ ${value1} = "get" ]]
then
        asterisk -rx "${commandget}" | cut -d " " -f 2
elif [[ ${value1} = "put" ]]
then
        asterisk -rx "${commandput}" | cut -d " " -f 3
fi