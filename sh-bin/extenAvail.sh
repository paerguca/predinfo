#!/bin/bash

asterisk -rx 'sip show inuse' | grep $1 | tr -s ' ' | cut -d " " -f 1,2 | cut -d "/" -f 1 | cut -d " " -f 2