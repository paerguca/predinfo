#!/bin/bash

verb=$1 # get o put
value0=$2 # idcall o uniqueid src(solo para el caso de consulta inversa)
value1=$3 # uniqueid src o "inv"
value2=$4 # uniqueid dst

commandputsrc="database put PRED_SRC ${value0} ${value1}"
commandputdst="database put PRED_DST ${value0} ${value2}"
commandputinv="database put PRED_INV ${value1} ${value0}" #se guarda de manera inversa para poder consultar por el idcall y tener como comparar en el evento End.
commandget="database get PRED_${value1^^} ${value0}"

if [[ ${verb} = "get" ]]
then
        asterisk -rx "${commandget}" | cut -d " " -f 2
elif [[ ${verb} = "put" ]]
then
        out1=`asterisk -rx "${commandputsrc}" | cut -d " " -f 3`
        out2=`asterisk -rx "${commandputdst}" | cut -d " " -f 3`
        out3=`asterisk -rx "${commandputinv}" | cut -d " " -f 3`
        echo $out1
fi